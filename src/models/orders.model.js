'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment');

    const db = mongoose.connection;
    autoIncrement.initialize(db);

var Order = new Schema({
    orderId: String,
    ClientName: String,
    FacilityCode: String,
    product : String
});

Order.plugin(autoIncrement.plugin, {model: 'Order', field: 'orderId'});

module.exports = mongoose.model('Orders', Order);