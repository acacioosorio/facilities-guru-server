const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
mongoose.connect('mongodb://facilitiesguro:facilitiesguro123@ds051738.mlab.com:51738/study-test');

const Orders = require('./models/orders.model');

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

app.get('/posts', (req, res) => {
	res.send(
		[{
			title: "Hello World!",
			description: "Hi there! How are you?"
		}]
	)
});

app.get('/orders', (req, res) => {
	Orders.find()
	.then((resp) =>{
		res.send(resp);
	})
	.catch(err => handleError(res, err) )
});

app.delete('/orders', (req, res) => {
	Orders.findByIdAndRemove({_id: req.query.id}, (err, docs) => {
		if(err) handleError(res, err);
		res.send(docs);
	});
});

app.get('/facility', (req, res) => {
	console.log(req.query);
	Orders.find({ ClientName : req.query.store }).select('FacilityCode -_id')
	.then((resp) =>{
		res.send(resp);
	})
	.catch(err => handleError(res, err) )
});

app.get('/seed', (req, res) => {

	let seed = {
		ClinteName: "Pichau",
		FacilityCode: "facility-sp-1",
		product : "DT3 GAMING CHAIR"
	}

	var savedSeed = new Orders(seed);

	savedSeed.save(function (err, seedSaved) {
		if (err) return handleError(res, err);
		res.send(seedSaved);
	});

});

function handleError(res, err) {
    return res.status(500).send(err);
}

app.listen(process.env.PORT || 8081)